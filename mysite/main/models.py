from django.db import models
from django.contrib.auth.models import User

class BookJournalBase(models.Model) :
    name = models.CharField(max_length=100)
    price = models.FloatField()
    description = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        abstract = True


class Book(BookJournalBase):
    num_pages = models.IntegerField(null=True)
    genre = models.CharField(max_length=255)

    def toJson(self):
        return{
            'name': self.name,
            'price': self.price,
            'description': self.description,
            'created': self.created,
            'num_pages': self.num_pages,
            'genre': self.genre,
        }

class Journal(BookJournalBase):
    Bullet = 'Bullet'
    Food = 'Food'
    Travel = 'Travel'
    Sport = 'Sport'

    CHOICES =(
        (Bullet, 'Bullet'),
        (Food, 'Food'),
        (Travel, 'Travel'),
        (Sport, 'Sport')
    )

    type = models.CharField(max_length=255, choices=CHOICES, default=Bullet)
    publisher = models.ForeignKey(User, on_delete=models.CASCADE)

    def toJson(self):
        return{
            'name': self.name,
            'price': self.price,
            'description': self.description,
            'created': self.created,
            'type': self.type,
            'publisher': self.publisher,
        }
