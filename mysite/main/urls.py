from django.urls import path, include

from .views import bookCreate, getBooks, bookEdit, bookUpdate, bookDelete, journalCreate, getJournals, journalEdit, journalUpdate, journalDelete

urlpatterns = [
    path('books/', bookCreate),
    path('books/list', getBooks),
    path('books/edit/<int:id>', bookEdit),
    path('books/update/<int:id>', bookUpdate),
    path('books/delete/<int:id>', bookDelete),
]

urlpatterns = [
    path('journals/', journalCreate),
    path('journals/list', getJournals),
    path('journals/edit/<int:id>', journalEdit),
    path('journals/update/<int:id>', journalUpdate),
    path('journals/delete/<int:id>', journalDelete),
]