from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.response import Response
from django import forms

from .models import Book, Journal

class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = "__all__"


def bookCreate(request):
    if request.method == "POST":
        form = BookForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return Response("Created")
            except:
                pass

    else:
        form = BookForm()
    return HttpResponse ({'form': form})

def getBooks(request):
    books = Book.objects.all()
    return HttpResponse([i.toJson() for i in books])

def bookEdit(request, id):
    book = Book.objects.get(id=id)
    return HttpResponse({'book': book})

def bookUpdate(request, id):
    book = Book.objects.get(id=id)
    form = BookForm(request.POST, instance=book)
    if form.is_valid():
        form.save()
        return Response("Updated")
    return HttpResponse({'book': book})


def bookDelete(request, id):
    employee = Book.objects.get(id=id)
    employee.delete()
    return Response("Deleted")


class JournalForm(forms.ModelForm):
    class Meta:
        model = Journal
        fields = "__all__"


def journalCreate(request):
    if request.method == "POST":
        form = JournalForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return Response("Created")
            except:
                pass

def getJournals(request):
    journals = Journal.get.all()
    return HttpResponse([i.toJson() for i in journals])

def journalEdit(request, id):
    permission_classes = [isAdminUser]
    journal = Journal.objects.get(id=id)
    return HttpResponse({'journal': journal})

def journalUpdate(request, id):
    permission_classes = [isAdminUser]
    journal = Journal.objects.get(id=id)
    form = JournalForm(request.POST, instance=journal)
    if form.is_valid():
        form.save()
        return Response("Updated")
    return HttpResponse({'journal': journal})


def journalDelete(request, id):
    permission_classes = [isAdminUser]
    journal = Journal.objects.get(id=id)
    journal.delete()
    return Response("Deleted")